port module Main exposing (..)

import Browser
import Array exposing (Array)
import Html exposing (Html, Attribute, input, div, text, label, button, ul, li, form, a, hr, p)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick, onSubmit)

main = Browser.document {init = init, update = updateWithCmd, view = view, subscriptions = subscriptions }

type alias Contact =
  { firstName : String
  , lastName : String
  , phone : String
  }

type alias Model = 
  { contactForm : Contact
  , contactList : Array Contact
  , selectedContact : Maybe Contact
  , contactIndex : Int
  }

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

init : Maybe (List Contact) -> (Model, Cmd msg)
init maybeContacts =
  ({ contactForm = Contact "" "" ""
  , contactList = 
    case maybeContacts of
      Just contacts ->
        Array.fromList contacts
      Nothing ->
        Array.empty
  , selectedContact = Nothing
  , contactIndex = 0
  }, Cmd.none)

type Msg
  = SetFirstName String
  | SetLastName String
  | SetPhone String
  | SaveContact
  | SelectContact Int
  | RemoveContact

updateFirstName : Contact -> String -> Contact
updateFirstName contact firstName =
  { contact | firstName = firstName }

updateLastName : Contact -> String -> Contact
updateLastName contact lastName =
  { contact | lastName = lastName }

updatePhone : Contact -> String -> Contact
updatePhone contact phone =
  { contact | phone = phone }

removeContact : Int -> Array Contact -> Array Contact
removeContact id contacts =
  let
    a1 = Array.slice 0 id contacts
    a2 = Array.slice (id + 1) (Array.length contacts) contacts
  in
    Array.append a1 a2

port setStorage : Array Contact -> Cmd msg

updateWithCmd : Msg -> Model -> (Model, Cmd msg)
updateWithCmd msg model =
  let
    newModel = update msg model
  in
    (newModel, setStorage newModel.contactList)

update : Msg -> Model -> Model
update msg model =
  case msg of
    SetFirstName firstName ->
      { model | contactForm = updateFirstName model.contactForm firstName }
    SetLastName lastName ->
      { model | contactForm = updateLastName model.contactForm lastName }
    SetPhone phone ->
      { model | contactForm = updatePhone model.contactForm phone }
    SaveContact ->
      if model.contactForm.firstName == "" || model.contactForm.lastName == "" then
        model
      else
        { model | contactList = Array.push model.contactForm model.contactList, contactForm = Contact "" "" "" }
    SelectContact id ->
      { model | selectedContact = (Array.get id model.contactList), contactIndex = id }
    RemoveContact ->
      { model | selectedContact = Nothing, contactList = removeContact model.contactIndex model.contactList }


viewContactForm : Contact -> Html Msg
viewContactForm contact =
  Html.form [ onSubmit SaveContact ]
  [ div [ class "form-group" ]
    [ label [ for "firstName" ] [ text "First Name" ]
    , input [ value contact.firstName, onInput SetFirstName, class "form-control", name "firstName" ] []
    ]
  , div [ class "form-group" ]
    [ label [ for "lastName" ] [ text "Last Name" ]
    , input [ value contact.lastName, onInput SetLastName, class "form-control", name "lastName" ] []
    ]
  , div [ class "form-group" ]
    [ label [ for "phone" ] [ text "Phone" ]
    , input [ value contact.phone, onInput SetPhone, class "form-control", name "phone" ] []
    ]
  , input [ type_ "submit", value "Add to contacts", class "btn btn-primary" ] []
  ]

viewContactList : List Contact -> Html Msg
viewContactList list =
  ul [ class "nav flex-column"] (List.indexedMap (\i l ->
    li [ class "nav-item" ]
    [ a [ class "nav-link", href "#", onClick (SelectContact i) ]
      [ text (l.firstName ++ " " ++ l.lastName)
      ]
    ]) list)

viewContact : Contact -> Html Msg
viewContact contact =
  div []
  [ p [] [ text ("First Name: " ++ contact.firstName) ]
  , p [] [ text ("Last Name: " ++ contact.lastName) ]
  , p [] [ text ("Phone: " ++ contact.phone) ]
  , button [ class "btn  btn-danger", onClick RemoveContact ] [ text "Remove this contact" ]
  ]

view : Model -> Browser.Document Msg
view model =
  { title = "Contacts"
  , body =
    [ div [ style "margin-top" "50px", class "container-fluid" ]
      [ div [ class "row" ]
        [ div [ class "col-sm-4"]
          [ viewContactForm model.contactForm
          , hr [] []
          , viewContactList (Array.toList model.contactList)
          ]
        , div [ class "col-sm-8" ]
          [
            case model.selectedContact of
              Just contact->
                viewContact contact
              Nothing ->
                text "Nothing"
          ]
        ]
      ]
    ]
  }
